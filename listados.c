//Implementacion de las funciones para crear y mantener los listados de clientes y creditos
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "listados.h"

int printFormulario(Cliente clienteNuevo){
    char confirmacion;

    printf("\n-----------------------------------------------------\n");
    printf("DATOS INGRESADOS");
    printf("\n-----------------------------------------------------\n");
    printf("NOMBRE: %s\n",&clienteNuevo.nombre);
    printf("APELLIDO: %s\n",&clienteNuevo.apellido);
    printf("EDAD: %i\n",clienteNuevo.edad);
    printf("DOCUMENTO: %i\n",clienteNuevo.dni);
    printf("-----------------------------------------------------\n\n");
    int charCorrecto = 0;

    while(charCorrecto == 0){
        printf("Si los datos ingresados son correctos ingresar Y, para reescribir los datos ingresar N:");
        scanf("%s",&confirmacion);

        if(confirmacion == 'Y' || confirmacion == 'y'){
            return 1;
        }else if(confirmacion == 'N' || confirmacion == 'n'){
            return 0;
        }else{
            printf("\nCARACTER INVALIDO...\n");
        }
    }
}

void agregarReferidoAlArchivo(Cliente clienteNuevo,Cliente *clienteReferido){
    FILE *archivo;
    archivo = fopen ( "ListadoCLIENTES/referidos.txt", "a+t" );

    char formulario[1024];

    strcpy(formulario, "");
    strncat(formulario, clienteNuevo.nombre, 1024);
    strncat(formulario, " ",1024);
    strncat(formulario, clienteNuevo.apellido, 1024);
    strncat(formulario, ":", 1024);
    strncat(formulario, clienteReferido->nombre, 1024);
    strncat(formulario, clienteReferido->apellido, 1024);
    strncat(formulario, "\n",1024);

    fputs(formulario, archivo);

    fclose (archivo);
}

Cliente crearCliente(ListaClientes *estaLista){
    Cliente clienteNuevo;
    int confirmar = 0;
    int confirmarReferido = 0;
    char agregarReferido;

    while(confirmar == 0){
        printf("Ingresar NOMBRE:");
        scanf("%s",&clienteNuevo.nombre);
        printf("Ingresar APELLIDO:");
        scanf("%s",&clienteNuevo.apellido);
        printf("Ingresar EDAD:");
        scanf("%i",&clienteNuevo.edad);
        printf("Ingresar DNI:");
        scanf("%i",&clienteNuevo.dni);

        confirmar = printFormulario(clienteNuevo);
    }

    printf("\nEl nuevo cliente cuenta con un referido?\nSi la respuesta es afirmativa ingresar Y, de lo contrario N.");
    scanf(" %c",&agregarReferido);
    if(agregarReferido == 'Y' || agregarReferido == 'y'){
        while(confirmarReferido == 0){
            int documentoReferido;
            printf("Ingresar documento del REFERIDO:");
            scanf("%d",&documentoReferido);

            while(estaLista != NULL && confirmarReferido == 0){
                if(estaLista->miCliente.dni == documentoReferido){
                    Cliente clienteReferido= estaLista->miCliente;

                    Cliente *punteroAlCliente = &clienteReferido;
                    Cliente *punteroAlClienteNuevo = &clienteNuevo;

                    punteroAlClienteNuevo->referido = punteroAlCliente;
                    punteroAlClienteNuevo->suCredito = NULL;

                    printf("\nEl referido es");
                    printf(" %s",estaLista->miCliente.nombre);
                    printf(" %s",estaLista->miCliente.apellido);
                    printf(" , presione ENTER para continuar...");
                    agregarReferidoAlArchivo(clienteNuevo,punteroAlClienteNuevo->referido);
                    getch();

                    confirmarReferido = 1;
                }
                estaLista = estaLista->nodoSiguiente;
            }
            if(confirmarReferido == 0){
                printf("El documento del referido ingresado no es correcto");
            }
        }
    }else if(agregarReferido == 'N' || agregarReferido == 'n'){
        Cliente *punteroAlClienteNuevo = &clienteNuevo;

        punteroAlClienteNuevo->referido = NULL;
        punteroAlClienteNuevo->suCredito = NULL;
    }else{
        printf("\nCARACTER INVALIDO...\n");
    }

    return clienteNuevo;
}

ListaClientes *inicializarLista(ListaClientes *nuevaLista){
    nuevaLista = NULL;
    return nuevaLista;
}

void agregarClienteAlArchivo(Cliente clienteNuevo){
    FILE *archivo;
    archivo = fopen ( "ListadoCLIENTES/fichero.txt", "a+t" );

    char formulario[1024];

    strcpy(formulario, "NOMBRE: ");
    strncat(formulario, clienteNuevo.nombre, 1024);

    strncat(formulario, "\nAPELLIDO: ", 1024);
    strncat(formulario, clienteNuevo.apellido, 1024);

    char edadSTR[100];
    strncat(formulario, "\nEDAD: ", 1024);
    sprintf(edadSTR, "%d", clienteNuevo.edad);
    strncat(formulario, edadSTR, 1024);

    char documentoSTR[100];
    strncat(formulario, "\nDOCUMENTO: ", 1024);
    sprintf(documentoSTR, "%d", clienteNuevo.dni);
    strncat(formulario, documentoSTR, 1024);

    strncat(formulario, "\n;\n", 1024);

    fputs(formulario, archivo);

    fclose (archivo);
}

ListaClientes *agregarCliente(ListaClientes *estaLista,Cliente value,int clienteNuevo){

    ListaClientes *nuevaLista,*auxiliar;
    nuevaLista= (ListaClientes*)malloc(sizeof(ListaClientes));
    nuevaLista->miCliente = value;
    nuevaLista->nodoSiguiente = NULL;
    if(estaLista == NULL){
        estaLista = nuevaLista;
    }else{
        auxiliar = estaLista;
        while(auxiliar->nodoSiguiente != NULL){
            auxiliar = auxiliar->nodoSiguiente;
        }
        auxiliar->nodoSiguiente = nuevaLista;
    }

    if(clienteNuevo == 1){
        agregarClienteAlArchivo(value);
    }
    return estaLista;
}

void buscarClientePorNombre(ListaClientes *estaLista){
    char nombreBuscado[20];
    int encontroCliente = 0;

    printf("Ingresar NOMBRE: ");
    scanf("%s",nombreBuscado);

    while(estaLista != NULL && encontroCliente == 0){
        char nombreRecuperado[20];
        char c;

        for(int i = 0 ; i < strlen(estaLista->miCliente.nombre) ; i++){
            if((c >= 'a' && c <= 'z' ) || (c >= 'A' && c <= 'Z' )){
                nombreRecuperado[i] = estaLista->miCliente.nombre[i+1];
            }
        }

        if(strcmp(nombreRecuperado,nombreBuscado) == 0 || strcmp(estaLista->miCliente.nombre,nombreBuscado) == 0){
            printf("\n------------------------------------------------------------------------------\n");
            printf("CLIENTE BUSCADO: ");
            printf("\n------------------------------------------------------------------------------\n");
            printf("NOMBRE: %s\n", estaLista->miCliente.nombre);
            printf("APELLIDO: %s\n", estaLista->miCliente.apellido);
            printf("EDAD: %i\n",estaLista->miCliente.edad);
            printf("DOCUMENTO: %i\n",estaLista->miCliente.dni);
            printf("------------------------------------------------------------------------------\n");

            encontroCliente = 1;
        }
        estaLista = estaLista->nodoSiguiente;
    }
    if(encontroCliente == 0){
        printf("\n\nEl cliente buscado no se encuentra dentro de los registros\n\n");
    }
    printf("\n\nPara volver al Menu principal presione ENTER... \n\n");
    char ch = getch();
}

void buscarClientePorRangoDeEdad(ListaClientes *estaLista){
    int *parametroMenor;
    int *parametroMayor;
    int encontro = 0 ;

    printf("Ingresar parametro menor: ");
    scanf("%d",&parametroMenor);
    printf("Ingresar parametro mayor: ");
    scanf("%d",&parametroMayor);

    while(estaLista != NULL){
        if(estaLista->miCliente.edad > parametroMenor && estaLista->miCliente.edad < parametroMayor){
            printf("\n");
            printf("------------------------------------------------------------------------------\n");
            printf("NOMBRE: %s\n", estaLista->miCliente.nombre);
            printf("APELLIDO: %s\n", estaLista->miCliente.apellido);
            printf("EDAD: %i\n",estaLista->miCliente.edad);
            printf("DOCUMENTO: %i\n",estaLista->miCliente.dni);
            printf("\n");

            encontro = 1;
        }
        estaLista = estaLista->nodoSiguiente;
    }
    if(encontro == 0){
        printf("\n\nNo se encontraron clientes con la descripcion presentada\n\n");
    }
    printf("\n\nPara volver al Menu principal presione ENTER... \n\n");
    getch();
}

//CREDITOS------------------------------------------------------------------------------------------------

int printFormularioDeCredito(Credito creditoNuevo,char nombre[],char apellido[]){
    char confirmacion;

    printf("\n-----------------------------------------------------\n");
    printf("DATOS INGRESADOS");
    printf("\n-----------------------------------------------------\n");
    printf("TITULAR:%s",nombre);
    printf("%s\n",apellido);
    printf("DOCUMENTO: %d\n",creditoNuevo.dni);
    printf("MONTO: %d\n",creditoNuevo.monto);
    printf("SALDO PENDIENTE: %d\n",creditoNuevo.monto);
    printf("-----------------------------------------------------\n\n");
    int charCorrecto = 0;

    while(charCorrecto == 0){
        printf("Si los datos ingresados son correctos ingresar Y, para reescribir los datos ingresar N:");
        scanf("%s",&confirmacion);

        if(confirmacion == 'Y' || confirmacion == 'y'){
            charCorrecto = 1;
            return 1;
        }else if(confirmacion == 'N' || confirmacion == 'n'){
            charCorrecto = 1;
            return 0;
        }else{
            printf("\nCARACTER INVALIDO...\n");
        }
    }

}

Credito crearCredito(ListaClientes *estaLista, ListaCreditos *estaListaCreditos){
    Credito creditoNuevo;
    int confirmar = 0;

    while(confirmar == 0){
        printf("Ingresar DNI:");
        scanf("%d",&creditoNuevo.dni);
        printf("Ingresar MONTO:");
        scanf("%d",&creditoNuevo.monto);

        int encontroCliente = 0 ;
        int existe = 0;
        while(estaLista != NULL && encontroCliente == 0){
            if(estaLista->miCliente.dni == creditoNuevo.dni){
                confirmar = printFormularioDeCredito(creditoNuevo,estaLista->miCliente.nombre,estaLista->miCliente.apellido);
                while(estaListaCreditos != NULL && existe == 0){
                    if(estaListaCreditos->miCredito.dni == creditoNuevo.dni){
                        existe = 1;
                    }
                    estaListaCreditos = estaListaCreditos->nodoSiguiente;
                }
                if(existe == 0){
                    Cliente clienteBuscado = estaLista->miCliente;
                    Cliente *punteroAlClienteBuscado = &clienteBuscado;

                    creditoNuevo.saldoPendiente = creditoNuevo.monto;
                    creditoNuevo.estado = activo;

                    punteroAlClienteBuscado->suCredito = &creditoNuevo;
                }
                encontroCliente = 1;
            }
            estaLista = estaLista->nodoSiguiente;
        }
        if(encontroCliente == 1 && existe == 1){
            printf("\nNo se puede agregar el credito debido a que ya existe uno con esa descripcion, ENTER AL MENU...\n\n");
            creditoNuevo.dni = NULL;

            getch();

        }
        if(confirmar == 0){
            printf("\nEl documento que se ingreso no coincide con ningun cliente. Ingresar documento correctos...\n\n");
        }
    }
    return creditoNuevo;
}

ListaCreditos *inicializarListaDeCreditos(ListaCreditos *nuevaLista){
    nuevaLista = NULL;
    return nuevaLista;
}

void agregarCreditoAlArchivo(Credito creditoNuevo){
    FILE *archivo;
    archivo = fopen ( "ListadoCLIENTES/ficheroDeCreditos.txt", "a+t" );

    char formulario[1024];

    char documentoSTR[100];
    strcpy(formulario, "DOCUMENTO: ");
    sprintf(documentoSTR, "%d", creditoNuevo.dni);
    strncat(formulario, documentoSTR, 1024);

    char montoSTR[100];
    strncat(formulario, "\nMONTO: ", 1024);
    sprintf(montoSTR, "%d", creditoNuevo.monto);
    strncat(formulario, montoSTR, 1024);

    char saldoPendienteSTR[100];
    strncat(formulario, "\nSALDO PENDIENTE: ", 1024);
    sprintf(saldoPendienteSTR, "%d", creditoNuevo.saldoPendiente);
    strncat(formulario, saldoPendienteSTR, 1024);

    strncat(formulario, "\nESTADO: ", 1024);
    strncat(formulario, "Activo", 1024);

    strncat(formulario, "\n;\n", 1024);

    fputs(formulario, archivo);

    fclose (archivo);
}

ListaCreditos *agregarCredito(ListaCreditos *estaLista,Credito value,int clienteNuevo){
    ListaCreditos *nuevaLista,*auxiliar;
    nuevaLista= (ListaCreditos*)malloc(sizeof(ListaCreditos));
    nuevaLista->miCredito = value;
    nuevaLista->nodoSiguiente = NULL;
    if(estaLista == NULL){
        estaLista = nuevaLista;
    }else{
        auxiliar = estaLista;
        while(auxiliar->nodoSiguiente != NULL){
            auxiliar = auxiliar->nodoSiguiente;
        }
        auxiliar->nodoSiguiente = nuevaLista;
    }
    if(clienteNuevo == 1 && value.dni != NULL){
        agregarCreditoAlArchivo(value);

        printf("\nEl credito se agrego al cliente correctamente, presionar ENTER para continuar al menu...\n");
    }

    return estaLista;
}

void darCreditosDeBaja(ListaCreditos *estaLista){
    ListaCreditos *creditos = estaLista;
    int documentoPasado;
    int encontroCredito = 0;

    printf("DOCUMENTO DEL TITULAR: ");
    scanf("%d",&documentoPasado);

    while(creditos != NULL && encontroCredito == 0){
        if(creditos->miCredito.dni == documentoPasado){
           printf("\nSe dio de baja el credito con numero de documento: ");
           printf("%d\n", documentoPasado);

           creditos->miCredito.estado = cancelado;

           printf("\n%i",creditos->miCredito.estado);

           encontroCredito = 1;
        }
        creditos = creditos->nodoSiguiente;
    }
}