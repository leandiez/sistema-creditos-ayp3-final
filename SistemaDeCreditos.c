#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
#include "listados.c"

void crearArchivoDeReferidos(){
    FILE *archivo;
    archivo = fopen ("ListadoCLIENTES/referidos.txt", "w" );
    fclose (archivo);
}

ListaClientes *convertirDatosDeReferidos(FILE *archivo,char caracter,ListaClientes *estaLista){
    ListaClientes *nuevaLista = estaLista;
    ListaClientes *nuevaListaReferidos = estaLista;

    int posicion = 0;
    int habilitacion = 0;
    int dato = 1;

    char nombreRecup[120];
    memset(nombreRecup, 0, 120);
    char apellidoRecup[120];
    memset(apellidoRecup, 0, 120);
    char nombreReferidoRecup[120];
    memset(nombreReferidoRecup, 0, 120);
    char apellidoReferidoRecup[120];
    memset(apellidoReferidoRecup, 0, 120);

    while((caracter = fgetc(archivo)) != EOF){
        if(caracter == ' ' && (habilitacion == 0 || habilitacion == 2)){
            posicion=0;
            dato++;
        }
        if(caracter == ' ' && habilitacion == 1){
            habilitacion++;
        }
        if(caracter == ':'){
            habilitacion++;
            posicion=0;
            dato++;
        }
        if(caracter == '\n'){

            while(nuevaLista != NULL){
                Cliente clienteBuscado = nuevaLista->miCliente;
                Cliente *punteroAlClienteBuscado = &clienteBuscado;

                char nombreRecuperado[20];
                char apellidoRecuperado[20];

                for(int i = 0 ; i < strlen(punteroAlClienteBuscado->nombre) ; i++){
                    nombreRecuperado[i] = punteroAlClienteBuscado->nombre[i+1];
                }
                for(int i = 0 ; i < strlen(punteroAlClienteBuscado->apellido) ; i++){
                    apellidoRecuperado[i] = punteroAlClienteBuscado->apellido[i+1];
                }
                if(strcmp(nombreRecuperado,nombreRecup) == 0
                   && strcmp(apellidoRecuperado,apellidoRecup) == 0){
                    // busca cliente referido
                    while(nuevaListaReferidos != NULL){
                        Cliente clienteReferido = nuevaListaReferidos->miCliente;
                        Cliente *punteroAlClienteReferido = &clienteReferido;

                        char nombreReferidoRecuperado[20];
                        char apellidoReferidoRecuperado[20];

                        for(int i = 0 ; i < strlen(punteroAlClienteReferido->nombre) ; i++){
                            nombreReferidoRecuperado[i] = punteroAlClienteReferido->nombre[i+1];
                        }
                        for(int i = 0 ; i < strlen(punteroAlClienteReferido->apellido) ; i++){
                            apellidoReferidoRecuperado[i] = punteroAlClienteReferido->apellido[i+1];
                        }
                        if(strcmp(nombreReferidoRecuperado,nombreReferidoRecup) == 0
                           && strcmp(apellidoReferidoRecuperado,apellidoReferidoRecup) == 0){
                            punteroAlClienteBuscado->referido = punteroAlClienteReferido;

                        }
                        nuevaListaReferidos = nuevaListaReferidos->nodoSiguiente;
                    }
                }
                nuevaLista = nuevaLista->nodoSiguiente;
            }

            memset(nombreRecup, 0, 120);
            memset(apellidoRecup, 0, 120);
            memset(nombreReferidoRecup, 0, 120);
            memset(apellidoReferidoRecup, 0, 120);

            dato=1;
            posicion=0;
            habilitacion=0;

            nuevaLista = estaLista;
            nuevaListaReferidos = estaLista;
        }
        if(caracter != ':' && caracter != ' ' && caracter != '\n'){
            if(dato == 1){
                nombreRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 2){
                apellidoRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 3){
                nombreReferidoRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 4){
                apellidoReferidoRecup[posicion] = caracter;
                posicion++;
            }
        }
    }
    return estaLista;
}

ListaClientes *leerArchivoRecuperandoReferidos(ListaClientes *estaLista){
    FILE *archivo;
    char caracter;
    archivo = fopen ("ListadoCLIENTES/referidos.txt", "r" );

    if (archivo==NULL) {
        crearArchivoDeReferidos();
    }else{
        estaLista = convertirDatosDeReferidos(archivo,caracter,estaLista);
    }
    fclose (archivo);

    return estaLista;
}

void crearArchivo(){
    FILE *archivo;
    archivo = fopen ("ListadoCLIENTES/fichero.txt", "w" );
    fclose (archivo);
}

ListaClientes *convertirDatos(FILE *archivo,char caracter,ListaClientes *estaLista){
    int habilitacion = 0;
    int posicion = 0;
    int dato = 1;

    char nombreRecup[120];
    memset(nombreRecup, 0, 120);
    char apellidoRecup[120];
    memset(apellidoRecup, 0, 120);
    char edadRecup[120];
    memset(edadRecup, 0, 120);
    char documentoRecup[120];
    memset(documentoRecup, 0, 120);

    while((caracter = fgetc(archivo)) != EOF){
        if(caracter == ':'){
            habilitacion = 1;
            dato++;
        }
        if(caracter == '\n'){
            habilitacion = 0;
            posicion = 0;
            dato++;
        }
        if(caracter == ';'){
            dato = 0;

            Cliente clienteRecuperado;
            Cliente *punteroClienteRecuperado = &clienteRecuperado;
            strcpy(punteroClienteRecuperado->nombre, nombreRecup);
            strcpy(punteroClienteRecuperado->apellido, apellidoRecup);
            int edadENTERO = atoi(edadRecup);
            punteroClienteRecuperado->edad = edadENTERO;
            int documentoENTERO = atoi(documentoRecup);
            punteroClienteRecuperado->dni = documentoENTERO;

            estaLista = agregarCliente(estaLista,clienteRecuperado,0);

            memset(nombreRecup, 0, 120);
            memset(apellidoRecup, 0, 120);
            memset(edadRecup, 0, 120);
            memset(documentoRecup, 0, 120);
        }
        if(habilitacion == 1 && caracter != ':'){
            if(dato == 2){
                nombreRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 4){
                apellidoRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 6){
                edadRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 8){
                documentoRecup[posicion] = caracter;
                posicion++;
            }
        }
    }
    return estaLista;
}

ListaClientes *leerArchivoRecuperandoDatos(){
    FILE *archivo;
    ListaClientes *nuevaLista = NULL;
    char caracter;
    archivo = fopen ("ListadoCLIENTES/fichero.txt", "r" );

    if (archivo==NULL) {
        nuevaLista = inicializarLista(nuevaLista);
        crearArchivo();
    }else{
        nuevaLista = convertirDatos(archivo,caracter,nuevaLista);
    }
    fclose (archivo);

    return nuevaLista;
}

void leerArchivoParaListar(){
    FILE *archivo;
    char caracter;
    int cantidadDeClientesListados = 0;

    archivo = fopen ("ListadoCLIENTES/fichero.txt", "r" );

    printf("\n\n");
    if (archivo == NULL){
        printf("\nNo tienes clientes en la lista. \n\n");
    }else{
        while((caracter = fgetc(archivo)) != EOF){
            printf("%c",caracter);
            if(caracter == ';'){
                cantidadDeClientesListados++;
            }
            if(cantidadDeClientesListados == 5){
                cantidadDeClientesListados = 0;
                printf("\n\nPara continuar viendo clientes presionar ENTER\n");
                getch();
            }
        }
    }
    printf("\n\nPara volver al Menu principal presione ENTER... \n\n");
    getch();

    fclose(archivo);
}

void crearArchivoDeCreditos(){
    FILE *archivo;
    archivo = fopen ("ListadoCLIENTES/ficheroDeCreditos.txt", "w" );
    fclose (archivo);
}

ListaCreditos *convertirDatosDeCreditos(FILE *archivo,char caracter,ListaCreditos *estaLista,ListaClientes *estaListaDeClientes){
    ListaClientes *auxListaCliente = estaListaDeClientes;

    int habilitacion = 0;
    int posicion = 0;
    int dato = 1;

    char documentoRecup[120];
    memset(documentoRecup, 0, 120);
    char montoRecup[120];
    memset(montoRecup, 0, 120);
    char saldoPendienteRecup[120];
    memset(saldoPendienteRecup, 0, 120);

    while((caracter = fgetc(archivo)) != EOF){
        if(caracter == ':'){
            habilitacion = 1;
            dato++;
        }
        if(caracter == '\n'){
            habilitacion = 0;
            posicion = 0;
            dato++;
        }
        if(caracter == ';'){
            dato = 0;

            Credito creditoRecuperado;
            Credito *punteroCreditoRecuperado = &creditoRecuperado;
            int documentoENTERO = atoi(documentoRecup);
            punteroCreditoRecuperado->dni = documentoENTERO;
            int montoENTERO = atoi(montoRecup);
            punteroCreditoRecuperado->monto = montoENTERO;
            int saldoPendienteENTERO = atoi(saldoPendienteRecup);
            punteroCreditoRecuperado->saldoPendiente = saldoPendienteENTERO;

            estaLista = agregarCredito(estaLista,creditoRecuperado,0);

            while(auxListaCliente != NULL){
                if(punteroCreditoRecuperado->dni == auxListaCliente->miCliente.dni){
                    auxListaCliente->miCliente.suCredito = punteroCreditoRecuperado;
                }
                auxListaCliente = auxListaCliente->nodoSiguiente;
            }

            memset(documentoRecup, 0, 120);
            memset(montoRecup, 0, 120);
            memset(saldoPendienteRecup, 0, 120);

            auxListaCliente = estaListaDeClientes;
        }
        if(habilitacion == 1 && caracter != ':'){
            if(dato == 2){
                documentoRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 4){
                montoRecup[posicion] = caracter;
                posicion++;
            }
            if(dato == 6){
                saldoPendienteRecup[posicion] = caracter;
                posicion++;
            }
        }
    }
    return estaLista;
}

ListaCreditos *leerArchivoRecuperandoDatosDeCreditos(ListaClientes *estaListaDeClientes){
    FILE *archivo;
    ListaCreditos *nuevaLista = NULL;
    char caracter;
    archivo = fopen ("ListadoCLIENTES/ficheroDeCreditos.txt", "r" );

    if (archivo==NULL) {
        nuevaLista = inicializarListaDeCreditos(nuevaLista);
        crearArchivoDeCreditos();
    }else{
        nuevaLista = convertirDatosDeCreditos(archivo,caracter,nuevaLista,estaListaDeClientes);
    }
    fclose (archivo);

    return nuevaLista;
}

void leerArchivoParaListarCreditos(){
    FILE *archivo;
    char caracter;
    int cantidadDeClientesListados = 0;

    archivo = fopen ("ListadoCLIENTES/ficheroDeCreditos.txt", "r" );

    printf("\n\n");
    if (archivo == NULL){
        printf("\nNo tienes clientes en la lista. \n\n");
    }else{
        while((caracter = fgetc(archivo)) != EOF){
            printf("%c",caracter);
            if(caracter == ';'){
                cantidadDeClientesListados++;
            }
            if(cantidadDeClientesListados == 5){
                cantidadDeClientesListados = 0;
                printf("\n\nPara continuar viendo clientes presionar ENTER\n");
                getch();
            }
        }
    }
    printf("\n\nPara volver al Menu principal presione ENTER... \n\n");
    char ch = getch();

    fclose(archivo);
}

void imprimirMenuPrincipal(){
    printf("\n");
    printf("Menu de sistema de creditos.\n");
    printf("\n");

    printf("1. Alta de cliente \n");
    printf("2. Listar clientes \n");
    printf("3. Buscar por nombre \n");
    printf("4. Buscar por rango de edad \n");
    printf("5. Alta de credito \n");
    printf("6. Listar creditos \n");
    printf("7. Cancelar credito \n");
    printf("8. Salir del programa \n");
    printf("\n");

    printf("Numero de opcion seleccionada: \n");
}

int main(){
    if (GetFileAttributes("ListadoCLIENTES") == -1) {
        CreateDirectory("ListadoCLIENTES",NULL);
    }
    ListaClientes *nuevaLista = leerArchivoRecuperandoDatos();
    ListaCreditos *nuevaListaDeCreditos = leerArchivoRecuperandoDatosDeCreditos(nuevaLista);
    leerArchivoRecuperandoReferidos(nuevaLista);

    int opcionSeleccionada;
    int salir = 0;
    while (salir == 0){
        imprimirMenuPrincipal();
        scanf("%d", &opcionSeleccionada);

        switch(opcionSeleccionada){
            case 1:
                nuevaLista = agregarCliente(nuevaLista,crearCliente(nuevaLista),1);

                break;
            case 2:
                leerArchivoParaListar();

                break;
            case 3:
                buscarClientePorNombre(nuevaLista);

                break;
            case 4:
                buscarClientePorRangoDeEdad(nuevaLista);

                break;
            case 5:
                nuevaListaDeCreditos = agregarCredito(nuevaListaDeCreditos,crearCredito(nuevaLista,nuevaListaDeCreditos),1);

                break;
            case 6:
                leerArchivoParaListarCreditos();

                break;
            case 7:
                darCreditosDeBaja(nuevaListaDeCreditos);

                break;
            case 8:
                salir = 1;

                break;
            default:
                printf("No se ingreso ningun valor correcto. Escriba un numero y pulse Enter.\n\n\n\n");

                break;
            }
    }
    printf("\nEst%c a punto de abandonandor el programa, para confirmar la operaci%cn presione ENTER. . .\n",160,162);
    char ch = getch();

    return 0;
}
