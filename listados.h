typedef struct unCliente{
    unsigned int dni;
    unsigned int edad;
    char nombre[20];
    char apellido[20];
    struct unCliente *referido;
    struct unCredito *suCredito;
}Cliente;

typedef struct unCredito{
    unsigned int dni;
    unsigned int monto;
    unsigned int saldoPendiente;
    enum {cancelado,activo} estado;
}Credito;

typedef struct ItemCliente {
    Cliente miCliente;
    struct ItemCliente *nodoSiguiente;
} ListaClientes;

typedef struct ItemCredito {
    Credito miCredito;
    struct ItemCredito *nodoSiguiente;
} ListaCreditos;
